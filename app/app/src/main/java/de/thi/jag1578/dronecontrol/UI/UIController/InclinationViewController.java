package de.thi.jag1578.dronecontrol.UI.UIController;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.Display;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView;

import de.thi.jag1578.dronecontrol.Data.DroneCommunicationController;
import de.thi.jag1578.dronecontrol.UI.GameObject;
import de.thi.jag1578.dronecontrol.UI.UIViews.InclinationIndicatorView;
import de.thi.jag1578.dronecontrol.UI.UIViews.StaticElementsTextView;


public class InclinationViewController extends DroneCommunicationController implements GameObject
{
	private double currentAltitude = 0;
	private Context                   context;
	private LinearLayout              gameWidgets;
	private LinearLayout              inclinationElementsLayout;
	private HeightGraphViewController heightGraphViewController;
	private GraphView                 inclinationGraphView;
	private StaticElementsTextView    altitudeTextView;
	private StaticElementsTextView    inclinationTextView;
	private InclinationIndicatorView  inclinationIndicatorView;


	public InclinationViewController(Context context, LinearLayout gameWidgets)
	{
		super();

		this.context = context;
		this.gameWidgets = gameWidgets;
		this.setUpInclinationElementsLayout();
	}

	public void update()
	{
		// Set current currentAltitude
		this.currentAltitude = this.dataLink.getZ();
		this.altitudeTextView.setValueText(this.parseDouble(this.currentAltitude));

		// Set current inclination
		this.inclinationTextView.setValueText(this.parseInt(this.dataLink.getI()) + "°");
		this.inclinationTextView.setUnitText(this.generateInclinationUnit(this.dataLink.getI()));

		// Tell the HeightGraphViewController to update
		this.heightGraphViewController.update();
	}

	public void draw(Canvas canvas)
	{
		// Tell the HeightGraphViewController to redraw
		this.heightGraphViewController.draw(canvas);

		this.inclinationIndicatorView.setAltitude(this.currentAltitude);
		this.inclinationIndicatorView.invalidate();
	}

	private void setUpInclinationElementsLayout()
	{
		WindowManager wm      = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display       display = wm.getDefaultDisplay();
		Point         size    = new Point();
		display.getSize(size);
		int screenWidth  = size.x;
		int screenHeight = size.y;

		int paddingLeft = (screenWidth - (((2 * 133) + (2 * 8) + 582))) / 2;

		// Configure the text-view layout for the text-view elements
		this.inclinationElementsLayout = new LinearLayout(this.context);
		this.inclinationElementsLayout.setOrientation(LinearLayout.HORIZONTAL);
		this.inclinationElementsLayout.setPadding(paddingLeft, 0, 0, 0);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 172);
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.topMargin = 224; // TODO: calculate dynamically
		this.gameWidgets.addView(this.inclinationElementsLayout, params);

		// Altitude TextView
		this.altitudeTextView = new StaticElementsTextView(this.context);
		this.altitudeTextView.setTitleText("ALT");
		this.altitudeTextView.setUnitText("Meter");
		params = new LinearLayout.LayoutParams(133, 114);
		params.topMargin = 172 - params.height;
		this.inclinationElementsLayout.addView(this.altitudeTextView, params);

		// Inclination GraphView
		this.inclinationGraphView = new GraphView(this.context);
		params = new LinearLayout.LayoutParams(404, 174);
		params.leftMargin = 8;
		this.heightGraphViewController = new HeightGraphViewController(this.context,
		                                                               this.gameWidgets,
		                                                               this.inclinationGraphView);
		this.inclinationElementsLayout.addView(this.inclinationGraphView, params);

		// Inclination Indicator View
		this.inclinationIndicatorView = new InclinationIndicatorView(this.context, this);
		params = new LinearLayout.LayoutParams(
				180,
				174);
		params.leftMargin = 0;
		this.inclinationElementsLayout.addView(
				this.inclinationIndicatorView,
				params);

		// Inclination TextView
		this.inclinationTextView = new StaticElementsTextView(this.context);
		this.inclinationTextView.setTitleText("Incli");
		params = new LinearLayout.LayoutParams(133, 114);
		params.leftMargin = 8;
		params.topMargin = 172 - params.height;
		this.inclinationElementsLayout.addView(this.inclinationTextView, params);

	}

	private String generateInclinationUnit(int inclination)
	{
		String returnString = "";

		if (inclination > 0 && inclination < 90)
		{
			returnString = "UP";
		}
		else if (inclination > 90)
		{
			returnString = "DOWN";
		}

		return returnString;
	}

}
