package de.thi.jag1578.dronecontrol.UI.UIController;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import de.thi.jag1578.dronecontrol.Data.DroneCommunicationController;
import de.thi.jag1578.dronecontrol.UI.GameObject;


public class HeightGraphViewController extends DroneCommunicationController implements GameObject
{
	public static  int backgroundColor      = Color.parseColor("#334f6a");
	private static int heightGraphLineColor = Color.parseColor("#b43c40");
	private static int groundGraphLineColor = Color.parseColor("#cd0a25");
	private static int maxXDataPoints       = 500;
	private Context                    context;
	private LinearLayout               gameWidgets;
	private GraphView                  inclinationGraphView;
	private LineGraphSeries<DataPoint> heightSeries;
	private LineGraphSeries<DataPoint> groundLineSeries;
	private final Handler mHandler = new Handler();
	private Runnable heightSeriesTimer;
	private double heightGraphLastXValue = 0d;
	private double groundGraphLastXValue = 0d;
	private double latestHeightValue     = 0;
	private double latestSpeedValue      = 0;

	public HeightGraphViewController(Context context, LinearLayout gameWidgets, GraphView graphView)
	{
		super();

		this.context = context;
		this.gameWidgets = gameWidgets;
		this.inclinationGraphView = graphView;
		//this.heightGraphLastXValue = HeightGraphViewController.maxXDataPoints / 2;
		this.heightGraphLastXValue = HeightGraphViewController.maxXDataPoints;
		this.groundGraphLastXValue = HeightGraphViewController.maxXDataPoints;
		this.setUpGraphView();
	}

	public void update()
	{
		// Get current height value
		this.latestHeightValue = this.dataLink.getZ();

		// Get current speed value
		this.latestSpeedValue = this.dataLink.getV();
	}

	public void draw(Canvas canvas)
	{
		//
	}


	private void setUpGraphView()
	{
		// Activate horizontal zooming and scrolling
		this.inclinationGraphView.getViewport().setScalable(true);
		// Activate horizontal scrolling
		this.inclinationGraphView.getViewport().setScrollable(true);
		// Activate horizontal and vertical zooming and scrolling
		this.inclinationGraphView.getViewport().setScalableY(true);
		// Activate vertical scrolling
		this.inclinationGraphView.getViewport().setScrollableY(true);


		// Set manual X bounds
		this.inclinationGraphView.getViewport().setXAxisBoundsManual(true);
		this.inclinationGraphView.getViewport().setMinX(0);
		this.inclinationGraphView.getViewport().setMaxX(HeightGraphViewController.maxXDataPoints);

		// Do not show any labels on the axis
		this.inclinationGraphView.getGridLabelRenderer().setHorizontalLabelsVisible(false);
		this.inclinationGraphView.getGridLabelRenderer().setVerticalLabelsVisible(false);
		// Do not show a grid
		this.inclinationGraphView.getGridLabelRenderer().setGridStyle(
				GridLabelRenderer.GridStyle.NONE);
		this.inclinationGraphView.getGridLabelRenderer().setHighlightZeroLines(true);

		this.inclinationGraphView.getGridLabelRenderer().setPadding(4);

		// Add a border around the graph view
		GradientDrawable border = new GradientDrawable();
		border.setColor(HeightGraphViewController.backgroundColor);
		border.setStroke(2, Color.WHITE);
		this.inclinationGraphView.setBackgroundDrawable(border);

		// Add data-series for ground
		this.groundLineSeries = new LineGraphSeries<>(new DataPoint[]{
				new DataPoint(0, 0),
				new DataPoint(HeightGraphViewController.maxXDataPoints, 0),
				});
		this.groundLineSeries.setThickness(3);
		this.groundLineSeries.setColor(HeightGraphViewController.groundGraphLineColor);
		this.inclinationGraphView.addSeries(this.groundLineSeries);

		// Configure the style of the data-series that visualizes the height values
		this.heightSeries = new LineGraphSeries<>();
		this.heightSeries.setThickness(1);
		this.heightSeries.setColor(HeightGraphViewController.heightGraphLineColor);
		this.inclinationGraphView.addSeries(this.heightSeries);

		heightSeriesTimer = new Runnable()
		{
			@Override
			public void run()
			{
				// Make the graph move on only, if the drone is moving!
				if(latestSpeedValue != 0)
				{
					heightGraphLastXValue += 1d;
					groundGraphLastXValue += 1d;
					heightSeries.appendData(
							new DataPoint(heightGraphLastXValue, latestHeightValue),
							true,
							HeightGraphViewController.maxXDataPoints);

					groundLineSeries.appendData(
							new DataPoint(groundGraphLastXValue, 0),
							true,
							HeightGraphViewController.maxXDataPoints);

				}

				mHandler.postDelayed(this, 100);
			}
		};

		mHandler.postDelayed(heightSeriesTimer, 1000);
	}

}
