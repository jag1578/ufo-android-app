package de.thi.jag1578.dronecontrol.UI.UIViews;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;

import de.thi.jag1578.dronecontrol.Data.MainThread;
import de.thi.jag1578.dronecontrol.UI.UIController.ControlsViewController;
import de.thi.jag1578.dronecontrol.UI.UIController.InclinationViewController;
import de.thi.jag1578.dronecontrol.UI.UIController.MapViewController;
import de.thi.jag1578.dronecontrol.UI.UIController.StaticElementsViewController;


/**
 * GamTaken from: https://github.com/romuva/game-loop/tree/master/app/src/main/java/com/ailaikvis/economywars/economywars
 * <p>
 * Created by D on 2017-11-17.
 */

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback
{
	private LinearLayout                 gameWidgets;
	private MainThread                   thread;
	private MapViewController            mapViewController;
	private StaticElementsViewController staticElementsViewController;
	private ControlsViewController       controlsViewController;
	private InclinationViewController    inclinationViewController;

	public GamePanel(Context context)
	{
		super(context);

		this.setUp();
	}

	public GamePanel(Context context, LinearLayout gameWidgets)
	{
		super(context);

		this.setUp();
		this.gameWidgets = gameWidgets;

		// Initialize ViewController
		this.mapViewController = new MapViewController();
		this.staticElementsViewController = new StaticElementsViewController(context, gameWidgets);
		this.controlsViewController = new ControlsViewController(context, gameWidgets);
		this.inclinationViewController = new InclinationViewController(context, gameWidgets);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		thread = new MainThread(getHolder(), this);

		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		boolean retry = true;
		while (true)
		{
			try
			{
				thread.setRunning(false);
				thread.join();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			retry = false;
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		return super.onTouchEvent(event);
	}

	public void update()
	{
		this.mapViewController.update();
		this.staticElementsViewController.update();
		this.inclinationViewController.update();
	}

	@Override
	public void draw(Canvas canvas)
	{
		super.draw(canvas);

		canvas.save();
		canvas.rotate(this.mapViewController.getDirectionForMapRotation(), canvas.getWidth() / 2, canvas.getHeight() / 2);

		this.mapViewController.draw(canvas);

		canvas.restore();
		this.staticElementsViewController.draw(canvas);

		this.inclinationViewController.draw(canvas);
	}

	private void setUp()
	{
		getHolder().addCallback(this);
		thread = new MainThread(getHolder(), this);
		setFocusable(true);
	}

}