package de.thi.jag1578.dronecontrol.UI.UIViews;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.Map;

import static java.lang.StrictMath.cos;

import de.thi.jag1578.dronecontrol.R;
import de.thi.jag1578.dronecontrol.UI.UIController.StaticElementsViewController;


public class StaticElementsView extends ScaledView
{
	private static int    compassCrossWidth               = 4;
	private static int    dirIndicatorWidth               = 2;
	private static int    compassCrossColor               = Color.parseColor("#1e3347");
	private static int    radarStrokeColor                = Color.parseColor("#92989f");
	private static int    dirIndicatorTextBackgroundColor = Color.parseColor("#334f6a");
	private static int    dirIndicatorTextColor           = Color.parseColor("#ffffff");
	private static int    dirIndicatorTextPaddingTop      = 30;
	private static String northTextIndicator              = "N";
	private static String southTextIndicator              = "S";
	private static String westTextIndicator               = "W";
	private static String eastTextIndicator               = "E";
	private        int    radarRange                      = 0;
	private int                          northTextPaddingLeft;
	private int                          southTextPaddingLeft;
	private int                          westTextPaddingLeft;
	private int                          eastTextPaddingLeft;
	private StaticElementsViewController parentViewController;
	private Paint                        radarStrokePaint;
	private Paint                        compassCrossPaint;
	private Paint                        dirIndicatorLinePaint;
	private Paint                        dirIndicatorTextPaint;
	private Paint                        dirIndicatorTextBackgroundPaint;
	private Paint                        dirIndicatorTextStrokePaint;
	private Bitmap                       dronePositionIndicatorBitmap;

	public StaticElementsView(StaticElementsViewController parentViewController, int radarRange)
	{
		super();

		this.parentViewController = parentViewController;
		this.setUp(radarRange);
	}

	public void setRadarRange(int radarRange)
	{
		this.radarRange = radarRange;
	}

	public void setDronePositionIndicatorBitmap(Bitmap dronePositionIndicatorBitmap)
	{
		this.dronePositionIndicatorBitmap = dronePositionIndicatorBitmap;
	}

	public void drawStaticElementsInCanvas(Canvas canvas)
	{
		// Draw Radar-Range-Indicator Stroke
		int canvasCenterX = canvas.getWidth() / 2;
		int canvasCenterY = canvas.getHeight() / 2;
//		float radius        = this.scaleValueAsFloat(this.radarRange);
//		canvas.drawCircle(canvasCenterX, canvasCenterY, radius, this.radarStrokePaint);

		canvas.save();
		canvas.rotate(
				this.parentViewController.getDirectionForMapRotation(),
				canvas.getWidth() / 2,
				canvas.getHeight() / 2);

		// Draw Compass-Cross
		int crossLength = (int) Math.round((canvasCenterX / cos(Math.toRadians(45))));

		Rect northSouthRect = new Rect(canvasCenterX - (StaticElementsView.compassCrossWidth / 2),
		                               canvasCenterY - crossLength,
		                               canvasCenterX + (StaticElementsView.compassCrossWidth / 2),
		                               canvasCenterY + crossLength
		);

		Rect westEastRect = new Rect(canvasCenterX - crossLength,
		                             canvasCenterY - (StaticElementsView.compassCrossWidth / 2),
		                             canvasCenterX + crossLength,
		                             canvasCenterY + (StaticElementsView.compassCrossWidth / 2)
		);

		// Draw the rect (line) from north to south
		canvas.drawRect(northSouthRect, this.compassCrossPaint);
		// Draw the rect (line) from west to east
		canvas.drawRect(westEastRect, this.compassCrossPaint);
		// Restore original canvas rotation
		canvas.restore();


		// Draw the direction indicator rect
		Rect directionIndicatorRect = new Rect(
				canvasCenterX - (StaticElementsView.dirIndicatorWidth / 2),
				0,
				canvasCenterX + (StaticElementsView.dirIndicatorWidth / 2),
				canvasCenterY
		);
		canvas.drawRect(directionIndicatorRect, this.dirIndicatorLinePaint);

		// Draw the direction indicator text rects
		canvas.save();
		canvas.rotate(
				this.parentViewController.getDirectionForMapRotation(),
				canvas.getWidth() / 2,
				canvas.getHeight() / 2);

		// North
		Rect northTextStrokeRect = new Rect(canvasCenterX - (98 / 2),
		                                    canvasCenterY - (398 + 44),
		                                    canvasCenterX + (98 / 2),
		                                    canvasCenterY - (398));

		Rect northTextBackgroundRect = new Rect(northTextStrokeRect.left + 1,
		                                        northTextStrokeRect.top + 1,
		                                        northTextStrokeRect.right - 1,
		                                        northTextStrokeRect.bottom - 1);

		// Draw the north stroke rect
		canvas.drawRect(northTextStrokeRect, this.dirIndicatorTextStrokePaint);
		// Draw the north background rect
		canvas.drawRect(northTextBackgroundRect, this.dirIndicatorTextBackgroundPaint);
		// Draw the text for north indication
		canvas.drawText(
				StaticElementsView.northTextIndicator,
				(northTextBackgroundRect.left + this.northTextPaddingLeft + 4),
				(northTextBackgroundRect.top + StaticElementsView.dirIndicatorTextPaddingTop),
				this.dirIndicatorTextPaint);

		// South
		Rect southTextStrokeRect = new Rect(canvasCenterX - (98 / 2),
		                                    canvasCenterY + (398 + 44),
		                                    canvasCenterX + (98 / 2),
		                                    canvasCenterY + (398));

		Rect southTextBackgroundRect = new Rect(southTextStrokeRect.left + 1,
		                                        southTextStrokeRect.top + 1,
		                                        southTextStrokeRect.right - 1,
		                                        southTextStrokeRect.bottom - 1);

		// Draw the south stroke rect
		canvas.drawRect(southTextStrokeRect, this.dirIndicatorTextStrokePaint);
		// Draw the south background rect
		canvas.drawRect(southTextBackgroundRect, this.dirIndicatorTextBackgroundPaint);
		// Draw the text for south indication
		canvas.drawText(
				StaticElementsView.southTextIndicator,
				(southTextBackgroundRect.left + this.southTextPaddingLeft + 4),
				(southTextBackgroundRect.top - 15),
				this.dirIndicatorTextPaint);

		// West
		Rect westTextStrokeRect = new Rect(canvasCenterX - (398 + 98),
		                                   canvasCenterY - (44 / 2),
		                                   canvasCenterX - (398),
		                                   canvasCenterY + (44 / 2));

		Rect westTextBackgroundRect = new Rect(westTextStrokeRect.left + 1,
		                                       westTextStrokeRect.top + 1,
		                                       westTextStrokeRect.right - 1,
		                                       westTextStrokeRect.bottom - 1);

		// Draw the west stroke rect
		canvas.drawRect(westTextStrokeRect, this.dirIndicatorTextStrokePaint);
		// Draw the west background rect
		canvas.drawRect(westTextBackgroundRect, this.dirIndicatorTextBackgroundPaint);
		// Draw the text for west indication
		canvas.drawText(
				StaticElementsView.westTextIndicator,
				(westTextBackgroundRect.left + this.westTextPaddingLeft + 8),
				(westTextBackgroundRect.top + StaticElementsView.dirIndicatorTextPaddingTop),
				this.dirIndicatorTextPaint);

		// East
		Rect eastTextStrokeRect = new Rect(canvasCenterX + (398),
		                                   canvasCenterY - (44 / 2),
		                                   canvasCenterX + (398 + 98),
		                                   canvasCenterY + (44 / 2));

		Rect eastTextBackgroundRect = new Rect(eastTextStrokeRect.left + 1,
		                                       eastTextStrokeRect.top + 1,
		                                       eastTextStrokeRect.right - 1,
		                                       eastTextStrokeRect.bottom - 1);

		// Draw the east stroke rect
		canvas.drawRect(eastTextStrokeRect, this.dirIndicatorTextStrokePaint);
		// Draw the east background rect
		canvas.drawRect(eastTextBackgroundRect, this.dirIndicatorTextBackgroundPaint);
		// Draw the text for east indication
		canvas.drawText(
				StaticElementsView.eastTextIndicator,
				(eastTextBackgroundRect.left + this.eastTextPaddingLeft + 3),
				(eastTextBackgroundRect.top + StaticElementsView.dirIndicatorTextPaddingTop),
				this.dirIndicatorTextPaint);

		// Restore original canvas rotation
		canvas.restore();

		// Draw Drone-Position-Indicator
		int  tileLength = MapView.roundCoordinate(MapView.tileSize * this.getScale());
		Rect src        = new Rect(0, 0, this.dronePositionIndicatorBitmap.getWidth() - 1, this.dronePositionIndicatorBitmap.getHeight() - 1);
		Rect dest       = new Rect(
				canvasCenterX - (tileLength/2),
				canvasCenterY - (tileLength/2),
				canvasCenterX - (tileLength/2) + tileLength,
				canvasCenterY - (tileLength/2) + tileLength);
		canvas.drawBitmap(this.dronePositionIndicatorBitmap, src, dest, null);
	}

	private void setUp(int radarRange)
	{
		this.setRadarRange(radarRange);

		// Configure the Radar-Paint for the dashed circle
		// Stroke
		this.radarStrokePaint = new Paint();
		this.radarStrokePaint.setStyle(Paint.Style.STROKE);
		this.radarStrokePaint.setColor(StaticElementsView.radarStrokeColor);
		this.radarStrokePaint.setStrokeWidth(2);
		DashPathEffect dashPath = new DashPathEffect(new float[]{12, 6}, (float) 0.0);
		this.radarStrokePaint.setPathEffect(dashPath);
		this.radarStrokePaint.setAntiAlias(true);

		// Compass-Cross
		this.compassCrossPaint = new Paint();
		this.compassCrossPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		this.compassCrossPaint.setColor(StaticElementsView.compassCrossColor);
		this.compassCrossPaint.setStrokeWidth(1);
		this.compassCrossPaint.setAntiAlias(true);

		// Direction Indicator Line
		this.dirIndicatorLinePaint = new Paint();
		this.dirIndicatorLinePaint.setStyle(Paint.Style.STROKE);
		this.dirIndicatorLinePaint.setColor(StaticElementsView.radarStrokeColor);
		this.dirIndicatorLinePaint.setStrokeWidth(1);
		dashPath = new DashPathEffect(new float[]{20, 20}, (float) 7);
		this.dirIndicatorLinePaint.setPathEffect(dashPath);
		this.dirIndicatorLinePaint.setAntiAlias(true);

		// Direction Indicator Text (N, E, S, W rects)
		// Text
		this.dirIndicatorTextPaint = new Paint();
		this.dirIndicatorTextPaint.setColor(StaticElementsView.dirIndicatorTextColor);
		this.dirIndicatorTextPaint.setTextSize(30);
		this.dirIndicatorTextPaint.setTextAlign(Paint.Align.CENTER);
		this.dirIndicatorTextPaint.setAntiAlias(true);

		// Direction Indicator Text (N, E, S, W rects)
		// Background
		this.dirIndicatorTextBackgroundPaint = new Paint();
		this.dirIndicatorTextBackgroundPaint.setStyle(Paint.Style.FILL);
		this.dirIndicatorTextBackgroundPaint.setColor(
				StaticElementsView.dirIndicatorTextBackgroundColor);
		this.dirIndicatorTextBackgroundPaint.setAntiAlias(true);

		// Direction Indicator Text (N, E, S, W rects)
		// Stroke
		this.dirIndicatorTextStrokePaint = new Paint();
		this.dirIndicatorTextStrokePaint.setStyle(Paint.Style.STROKE);
		this.dirIndicatorTextStrokePaint.setColor(StaticElementsView.compassCrossColor);
		this.dirIndicatorTextStrokePaint.setStrokeWidth(2);
		this.dirIndicatorTextStrokePaint.setAntiAlias(true);

		// Calculate coordinates for text drawing
		// North
		int width = (int) this.dirIndicatorTextPaint.measureText(
				StaticElementsView.northTextIndicator);
		this.northTextPaddingLeft = ((98 - (2 * 2)) - (width / 2)) / 2;
		// South
		width = (int) this.dirIndicatorTextPaint.measureText(StaticElementsView.southTextIndicator);
		this.southTextPaddingLeft = ((98 - (2 * 2)) - (width / 2)) / 2;
		// West
		width = (int) this.dirIndicatorTextPaint.measureText(StaticElementsView.westTextIndicator);
		this.westTextPaddingLeft = ((98 - (2 * 2)) - (width / 2)) / 2;
		// East
		width = (int) this.dirIndicatorTextPaint.measureText(StaticElementsView.eastTextIndicator);
		this.eastTextPaddingLeft = ((98 - (2 * 2)) - (width / 2)) / 2;


	}


}
