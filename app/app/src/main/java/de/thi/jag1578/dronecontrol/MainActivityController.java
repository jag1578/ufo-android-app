package de.thi.jag1578.dronecontrol;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import de.thi.jag1578.dronecontrol.UI.UIViews.GamePanel;


public class MainActivityController extends Activity
{
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		                     WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);


		FrameLayout  game        = new FrameLayout(this);
		LinearLayout gameWidgets = new LinearLayout(this);
		gameWidgets.setOrientation(LinearLayout.VERTICAL);
		GamePanel    gameView    = new GamePanel(this, gameWidgets);

		game.addView(gameView);
		game.addView(gameWidgets);

		setContentView(game);
		ViewGroup.LayoutParams params = gameWidgets.getLayoutParams();
		params.height = ViewGroup.LayoutParams.MATCH_PARENT;
		params.width = ViewGroup.LayoutParams.MATCH_PARENT;
		gameWidgets.setLayoutParams(params);
	}

}
