package de.thi.jag1578.dronecontrol.UI.UIViews;

import android.graphics.Color;
import android.graphics.Paint;


public class ObjectiveTile extends Tile
{
	private static int tileColor = Color.parseColor("#cd0a25");

	public ObjectiveTile(double x, double y)
	{
		super(x, y);
	}

	public Paint setUpPaint()
	{
		Paint p = new Paint();
		p.setStyle(Paint.Style.FILL);
		p.setColor(tileColor);

		return p;
	}

	public boolean getIsCircleTile()
	{
		return true;
	}
}
