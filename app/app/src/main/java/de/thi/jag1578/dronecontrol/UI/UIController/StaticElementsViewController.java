package de.thi.jag1578.dronecontrol.UI.UIController;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.LinearLayout;

import de.thi.jag1578.dronecontrol.Data.DroneCommunicationController;
import de.thi.jag1578.dronecontrol.R;
import de.thi.jag1578.dronecontrol.UI.GameObject;
import de.thi.jag1578.dronecontrol.UI.UIViews.StaticElementsTextView;
import de.thi.jag1578.dronecontrol.UI.UIViews.StaticElementsView;


public class StaticElementsViewController extends DroneCommunicationController implements GameObject
{
	private Context                context;
	private LinearLayout           gameWidgets;
	private LinearLayout           textViewLayout;
	private StaticElementsView     staticElementsView;
	private StaticElementsTextView xCoordinateTextView;
	private StaticElementsTextView yCoordinateTextView;
	private StaticElementsTextView zCoordinateTextView;
	private StaticElementsTextView speedTextView;
	private StaticElementsTextView pitchTextView;
	private StaticElementsTextView directionTextView;
	private double                 lastZ;
	private long                   lastZTimestamp;
	private int                    tickCount;
	private double lastPitchSpeed = 0;

	public StaticElementsViewController(Context context, LinearLayout gameWidgets)
	{
		super();

		this.context = context;
		this.gameWidgets = gameWidgets;
		this.setUpTextViews();
		this.staticElementsView = new StaticElementsView(this, 110);
		this.staticElementsView.setDronePositionIndicatorBitmap(BitmapFactory.decodeResource(
				context.getResources(),
				R.drawable.drone_position_indicator));
	}

	public void update()
	{
		//
	}

	public void draw(Canvas canvas)
	{
		// Tell the Map what the scale at which to draw
		this.staticElementsView.setScale(this.getCurrentScale());

		// Re-draw the Static Elements
		this.staticElementsView.drawStaticElementsInCanvas(canvas);

		// Set current coordinates
		this.xCoordinateTextView.setValueText(this.parseDouble(this.dataLink.getX()));
		this.yCoordinateTextView.setValueText(this.parseDouble(this.dataLink.getY()));
		this.zCoordinateTextView.setValueText(this.parseDouble(this.dataLink.getZ()));

		// Set current speed
		this.speedTextView.setValueText(this.parseDouble(this.dataLink.getV()));

		// Set current pitch
		double pitchSpeed = this.calculatePitchSpeed(this.dataLink.getI());
		this.pitchTextView.setValueText(this.parseDouble(pitchSpeed));
		this.pitchTextView.setUnitText(this.generatePitchUnit(pitchSpeed));

		// Set current heading direction
		int headingDirection = this.dataLink.getD();
		this.directionTextView.setUnitText(this.generateDirectionUnit(headingDirection));

		if (headingDirection == 0)
		{
			headingDirection = 360;
		}

		this.directionTextView.setValueText(this.parseInt(headingDirection) + "°");
	}

	private void setUpTextViews()
	{
		WindowManager wm      = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display       display = wm.getDefaultDisplay();
		Point         size    = new Point();
		display.getSize(size);
		int screenWidth  = size.x;
		int screenHeight = size.y;

		// Configure the text-view layout for the text-view elements
		this.textViewLayout = new LinearLayout(this.context);
		this.textViewLayout.setOrientation(LinearLayout.HORIZONTAL);
		this.textViewLayout.setPadding(0, 0, 0, 0);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 114);
		params.gravity = Gravity.CENTER_HORIZONTAL;
		this.gameWidgets.addView(this.textViewLayout, params);

		// X-Coordinate TextView
		this.xCoordinateTextView = new StaticElementsTextView(this.context);
		this.xCoordinateTextView.setTitleText("X");
		params = new LinearLayout.LayoutParams(133, 114);
		this.textViewLayout.addView(this.xCoordinateTextView, params);

		// Y-Coordinate TextView
		this.yCoordinateTextView = new StaticElementsTextView(this.context);
		this.yCoordinateTextView.setTitleText("Y");
		params = new LinearLayout.LayoutParams(133, 114);
		params.leftMargin = 2;
		this.textViewLayout.addView(this.yCoordinateTextView, params);

		// Z-Coordinate TextView
		this.zCoordinateTextView = new StaticElementsTextView(this.context);
		this.zCoordinateTextView.setTitleText("Z");
		params = new LinearLayout.LayoutParams(133, 114);
		params.leftMargin = 2;
		this.textViewLayout.addView(this.zCoordinateTextView, params);

		// Speed TextView
		this.speedTextView = new StaticElementsTextView(this.context);
		this.speedTextView.setTitleText("Airspeed");
		this.speedTextView.setUnitText("m/s");
		params = new LinearLayout.LayoutParams(133, 114);
		params.leftMargin = screenWidth - (2 * ((3 * params.width) + (2 * 2)));
		this.textViewLayout.addView(this.speedTextView, params);

		// Pitch TextView
		this.pitchTextView = new StaticElementsTextView(this.context);
		this.pitchTextView.setTitleText("V. Speed");
		params = new LinearLayout.LayoutParams(133, 114);
		params.leftMargin = 2;
		this.textViewLayout.addView(this.pitchTextView, params);

		// Direction TextView
		this.directionTextView = new StaticElementsTextView(this.context);
		this.directionTextView.setTitleText("Direction");
		params = new LinearLayout.LayoutParams(133, 114);
		params.leftMargin = 2;
		this.textViewLayout.addView(this.directionTextView, params);
	}

	private String generateDirectionUnit(int direction)
	{
		if (direction <= 22.5 || direction >= 337.5)
		{
			return "N";
		}
		else if (direction <= 67.5 && direction >= 22.5)
		{
			return "NE";
		}
		else if (direction <= 112.5 && direction >= 67.5)
		{
			return "E";
		}
		else if (direction <= 157.5 && direction >= 112.5)
		{
			return "SE";
		}
		else if (direction <= 202.5 && direction >= 157.5)
		{
			return "S";
		}
		else if (direction <= 247.5 && direction >= 202.5)
		{
			return "SW";
		}
		else if (direction <= 292.5 && direction >= 247.52)
		{
			return "W";
		}
		else if (direction <= 337.5 && direction >= 247.52)
		{
			return "NW";
		}


		return "";
	}

	private double calculatePitchSpeed(int inclination)
	{
		this.tickCount++;

		if (this.tickCount == 20)
		{
			double deltaZ = this.dataLink.getZ() - this.lastZ;
			long   deltaT = System.currentTimeMillis() - this.lastZTimestamp;

			if (deltaZ == 0)
			{
				this.lastPitchSpeed = 0;
			}
			else
			{
				// Attention: deltaT is milli-seconds, display shall me meters per second!
				this.lastPitchSpeed = (deltaZ / (deltaT / 1000.0));
			}

			// Update for next calculation
			this.lastZ = this.dataLink.getZ();
			this.lastZTimestamp = System.currentTimeMillis();

			// Reset the tick-count
			this.tickCount = 0;
		}

		return this.lastPitchSpeed;
	}

	private String generatePitchUnit(double pitchSpeed)
	{
		String returnString = "";

		if (pitchSpeed > 0)
		{
			returnString = "UP - ";
		}
		else if (pitchSpeed < 0)
		{
			returnString = "DOWN - ";
		}

		return returnString + "m/s";
	}

}
