package de.thi.jag1578.dronecontrol.UI.UIViews;


import android.graphics.Paint;


abstract public class Tile
{
	private Paint paint;
	private boolean isCircleTile = false;
	private boolean shallBeDrawn = true;
	private double simulationDataX;
	private double simulationDataY;

	abstract protected Paint setUpPaint();


	Tile(double x, double y)
	{
		super();

		this.setX(x);
		this.setY(y);

		this.paint = this.setUpPaint();
	}

	public Paint getPaint()
	{
		return this.paint;
	}

	public boolean getIsCircleTile()
	{
		return this.isCircleTile;
	}

	public double getX()
	{
		return this.simulationDataX;
	}

	public double getY()
	{
		return this.simulationDataY;
	}

	public boolean getShallBeDrawn()
	{
		return this.shallBeDrawn;
	}

	public void setX(double x)
	{
		this.simulationDataX = x;
	}

	public void setY(double y)
	{
		this.simulationDataY = y;
	}

	public void setShallBeDrawn(boolean shallBeDrawn)
	{
		this.shallBeDrawn = shallBeDrawn;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Tile tile = (Tile) o;

		if (isCircleTile != tile.isCircleTile)
		{
			return false;
		}
		if (Double.compare(tile.simulationDataX, simulationDataX) != 0)
		{
			return false;
		}
		if (Double.compare(tile.simulationDataY, simulationDataY) != 0)
		{
			return false;
		}
		return getPaint().equals(tile.getPaint());
	}

	@Override
	public int hashCode()
	{
		int  result;
		long temp;
		result = getPaint().hashCode();
		result = 31 * result + (isCircleTile ? 1 : 0);
		temp = Double.doubleToLongBits(simulationDataX);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(simulationDataY);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
