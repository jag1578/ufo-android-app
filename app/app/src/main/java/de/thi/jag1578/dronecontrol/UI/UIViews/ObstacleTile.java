package de.thi.jag1578.dronecontrol.UI.UIViews;


import android.graphics.Color;
import android.graphics.Paint;


public class ObstacleTile extends Tile
{
	private static int tileColor       = Color.parseColor("#d8ae3b");

	public ObstacleTile(double x, double y)
	{
		super(x, y);
	}

	public Paint setUpPaint()
	{
		Paint p = new Paint();
		p.setStyle(Paint.Style.FILL);
		p.setColor(tileColor);

		return p;
	}

}
