package de.thi.jag1578.dronecontrol.UI.UIViews;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.ArrayList;

import static java.lang.StrictMath.cos;


public class MapView extends ScaledView
{
	public static  int    tileSize                  = 64;
	private static int    backgroundColor           = Color.parseColor("#1a2937");
	private static int    radarBackgroundColor      = Color.parseColor("#3c5a7b");
	private        int    radarRange                = 0;
	private        double lastDroneTrackTileCenterX = 0;
	private        double lastDroneTrackTileCenterY = 0;
	private ArrayList<Tile> tileArrayList;
	private DroneTile       droneTile;
	private Paint           radarFillPaint;

	public MapView(int radarRange)
	{
		super();

		this.tileArrayList = new ArrayList<>();
		this.setUp(radarRange);
	}

	public void setRadarRange(int radarRange)
	{
		this.radarRange = radarRange;
	}

	public void setTileArrayList(ArrayList<Tile> tiles)
	{
		this.tileArrayList = tiles;
	}

	public void setDroneTile(DroneTile droneTile)
	{
		this.droneTile = droneTile;
	}

	public void drawMapInCanvas(Canvas canvas)
	{
		// Clear map
		canvas.drawColor(MapView.backgroundColor);

		// Draw Radar-Background
		// Draw Radar-Range-Indicator Background
//		int   canvasCenterX = canvas.getWidth() / 2;
//		int   canvasCenterY = canvas.getHeight() / 2;
//		float radius        = (float)(this.radarRange * this.getScale() * (MapView.tileSize / 2));
//		canvas.drawCircle(canvasCenterX, canvasCenterY, radius - 1, this.radarFillPaint);

		this.lastDroneTrackTileCenterX = 0;
		this.lastDroneTrackTileCenterY = 0;

		// Draw Map
		// First: Track, Obstacles and Objectives
		for (Tile currentTile : this.tileArrayList)
		{
			this.drawTile(canvas, currentTile);
		}

		// Finally, the Drone itself as it has to be visible above the other tiles
		this.drawTile(canvas, this.droneTile);

	}

	private void drawTile(Canvas canvas, Tile currentTile)
	{
		double centerX            = this.getCanvasX(currentTile.getY(), canvas.getWidth());
		double centerY            = (this.getCanvasY(currentTile.getX(), canvas.getHeight()));
		double scaledTileSizeHalf = ((MapView.tileSize * this.getScale()) / 2);

		double length = canvas.getWidth() / (cos(Math.toRadians(45)));

		double canvasWidthPadding  = (length - canvas.getWidth()) / 2;
		double canvasHeightPadding = (length - canvas.getHeight()) / 2;


		double canvasMarginLeft   = -canvasWidthPadding;
		double canvasMarginRight  = (2 * canvasWidthPadding) + canvas.getWidth();
		double canvasMarginTop    = -canvasHeightPadding;
		double canvasMarginBottom = (2 * canvasHeightPadding) + canvas.getHeight();

		//System.out.println("Simu-X: " + currentTile.getX() + " Canvas-X: " + centerX);
		//System.out.println("Simu-Y: " + currentTile.getY() + " Canvas-Y: " + centerY);

		// Check whether Tile would be visible within the canvas at all
		if (!((centerX + scaledTileSizeHalf) >= canvasMarginLeft && (centerX - scaledTileSizeHalf) <= canvasMarginRight
				&& (centerY + scaledTileSizeHalf) >= canvasMarginTop && (centerY - scaledTileSizeHalf) <= canvasMarginBottom))
		{
			// Tile would not be visible atm, so skip it
			return;
		}

		// Detect whether the track tile overlaps with other tiles
		/*if (currentTile instanceof DroneTrackTile)
		{
			if (!(((centerX - this.lastDroneTrackTileCenterX)
					>= (MapView.tileSize * this.getScale()))
					|| ((centerY - this.lastDroneTrackTileCenterY)
					>= (MapView.tileSize * this.getScale()))))
			{
				// Do NOT draw track tile because it is overlapping
				currentTile.setShallBeDrawn(false);
				return;
			}
			else
			{
				System.out.println("DO DRAW: deltaX: " + (centerX - this.lastDroneTrackTileCenterX) + " deltaY: " + (centerY - this.lastDroneTrackTileCenterY) + " TileSize: " + (MapView.tileSize * this.getScale()));
				this.lastDroneTrackTileCenterX = centerX;
				this.lastDroneTrackTileCenterY = centerY;
			}

		}
*/
		// Check which geometric-form shall be drawn
		if (!currentTile.getIsCircleTile())
		{
			// Draw a rect
			Rect dest = new Rect(MapView.roundCoordinate(centerX - scaledTileSizeHalf),
			                     MapView.roundCoordinate(centerY - scaledTileSizeHalf),
			                     MapView.roundCoordinate(centerX + scaledTileSizeHalf),
			                     MapView.roundCoordinate(centerY + scaledTileSizeHalf)
			);
			canvas.drawRect(dest, currentTile.getPaint());
		}
		else
		{
			// Draw a circle
			canvas.drawCircle(
					(float) centerX,
					(float) centerY,
					(float) scaledTileSizeHalf, currentTile.getPaint()
			                 );
		}

	}

	private void setUp(int radarRange)
	{
		this.setRadarRange(radarRange);

		// Configure the Radar-Paint for the dashed circle
		// Background
		this.radarFillPaint = new Paint();
		this.radarFillPaint.setStyle(Paint.Style.FILL);
		this.radarFillPaint.setColor(MapView.radarBackgroundColor);
		this.radarFillPaint.setAntiAlias(true);
	}

	private double getCanvasX(double simulationDataY, int canvasHeight)
	{
		double offset = simulationDataY - this.droneTile.getY();
		offset = offset * this.getScale() * MapView.tileSize;
		offset = offset + (canvasHeight / 2);

		return offset;
	}

	private double getCanvasY(double simulationDataX, int canvasWidth)
	{
		double offset = simulationDataX - this.droneTile.getX();
		offset = offset * this.getScale() * -1 * MapView.tileSize;
		offset = offset + (canvasWidth / 2);

		return offset;
	}

	public static int roundCoordinate(double coordinate)
	{
		return (int) Math.round(coordinate);
	}

}
