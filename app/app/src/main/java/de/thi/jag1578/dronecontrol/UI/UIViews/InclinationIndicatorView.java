package de.thi.jag1578.dronecontrol.UI.UIViews;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

import de.thi.jag1578.dronecontrol.UI.UIController.InclinationViewController;

import static java.lang.StrictMath.cos;


public class InclinationIndicatorView extends View
{
	private static int    inlcIndicatorViewBackgroundColor = Color.parseColor("#334f6a");
	private static int    inlcIndicatorLineColor           = Color.parseColor("#ffffff");
	private static int    inlcIndicatorGroundLineColor     = Color.parseColor("#cd0a25");
	private static int    radarConeBackgroundColor         = Color.parseColor("#563c58");
	private        double altitude                         = 0;
	private InclinationViewController parentViewController;
	private Paint                     borderPaint;
	private Paint                     defaultInclIndicatorLinePaint;
	private Paint                     inclIndicatorLinePaint;
	private Paint                     radarConePaint;
	private Paint                     groundLinePaint;

	public InclinationIndicatorView(Context context,
	                                InclinationViewController inclinationViewController)
	{
		super(context);
		this.parentViewController = inclinationViewController;
		this.setUp();
	}

	public void setAltitude(double altitude)
	{
		this.altitude = altitude;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		if (canvas == null)
		{
			return;
		}

		super.onDraw(canvas);

		int canvasCenterX = canvas.getWidth() / 2;
		int canvasCenterY = canvas.getHeight() / 2;

		int canvasHeightHalfMinusLineHeight = MapView.roundCoordinate(((canvas.getHeight() - 2) / 2) + 6);

		// Draw upper border
		Rect borderTop = new Rect(0, 0, canvas.getWidth(), 2);
		canvas.drawRect(borderTop, this.borderPaint);

		// Draw right border
		Rect borderRight = new Rect(canvas.getWidth() - 2, 0, canvas.getWidth(), canvas.getHeight());
		canvas.drawRect(borderRight, this.borderPaint);

		// Draw 0° indicator line
		Rect indlinationDefaultLine = new Rect(0, canvasHeightHalfMinusLineHeight, canvas.getWidth(), canvasHeightHalfMinusLineHeight + 1);
		canvas.drawRect(indlinationDefaultLine, this.defaultInclIndicatorLinePaint);

		int inclIndicatorLineLenght = (int) Math.round((canvasCenterX / cos(Math.toRadians(45))));
		inclIndicatorLineLenght = inclIndicatorLineLenght + canvasCenterX;

		canvas.save();
		canvas.rotate(
				this.parentViewController.getInclinationForInclinationIndicatorRotation(),
				canvas.getWidth() / 2,
				(canvas.getHeight() / 2) + 6);

		// Draw actual inclination indicator line
		Rect indlinationIndicatorLine = new Rect(canvasCenterX, canvasHeightHalfMinusLineHeight, inclIndicatorLineLenght, canvasHeightHalfMinusLineHeight+ 1);
		canvas.drawRect(indlinationIndicatorLine, this.inclIndicatorLinePaint);

		// Draw the radar-cone
		// Define the radius
		float radarConeHeight = 77;

		float j = (canvas.getWidth() - (radarConeHeight * 2)) / 2;
		float g = ((canvas.getHeight() - (radarConeHeight * 2)) / 2) + 6;

		RectF coneRect = new RectF(g,
		                           j,
		                           j + (radarConeHeight * 2),
		                           g + (radarConeHeight * 2));
		canvas.drawArc(coneRect, 25, 130, true, this.radarConePaint);


		canvas.restore();

		// Draw ground line if visible
		if (this.altitude <= 110)
		{
			int groundLineMarginTop = canvasCenterY + (canvasCenterY ) - 5;

			Rect groundLineRect = new Rect(
					0,
					groundLineMarginTop,
					canvas.getWidth() - 2,
					groundLineMarginTop + 3);
			canvas.drawRect(groundLineRect, this.groundLinePaint);
		}

	}

	private void setUp()
	{
		this.setBackgroundColor(InclinationIndicatorView.inlcIndicatorViewBackgroundColor);

		this.borderPaint = new Paint();
		this.borderPaint.setStyle(Paint.Style.FILL);
		this.borderPaint.setColor(Color.WHITE);
		this.borderPaint.setAntiAlias(true);

		this.groundLinePaint = new Paint();
		this.groundLinePaint.setStyle(Paint.Style.FILL);
		this.groundLinePaint.setColor(InclinationIndicatorView.inlcIndicatorGroundLineColor);
		this.groundLinePaint.setAntiAlias(true);

		this.radarConePaint = new Paint();
		this.radarConePaint.setStyle(Paint.Style.FILL);
		this.radarConePaint.setColor(InclinationIndicatorView.radarConeBackgroundColor);
		this.radarConePaint.setAntiAlias(true);

		this.defaultInclIndicatorLinePaint = new Paint();
		this.defaultInclIndicatorLinePaint.setStyle(Paint.Style.STROKE);
		this.defaultInclIndicatorLinePaint.setColor(InclinationIndicatorView.inlcIndicatorLineColor);
		this.defaultInclIndicatorLinePaint.setStrokeWidth(1);
		DashPathEffect dashPath = new DashPathEffect(new float[]{10, 10}, (float) 4);
		this.defaultInclIndicatorLinePaint.setPathEffect(dashPath);
		this.defaultInclIndicatorLinePaint.setAntiAlias(true);

		this.inclIndicatorLinePaint = new Paint();
		this.inclIndicatorLinePaint.setStyle(Paint.Style.STROKE);
		this.inclIndicatorLinePaint.setColor(InclinationIndicatorView.inlcIndicatorLineColor);
		this.inclIndicatorLinePaint.setStrokeWidth(1);
		dashPath = new DashPathEffect(new float[]{10, 10}, (float) 7);
		this.inclIndicatorLinePaint.setPathEffect(dashPath);
		this.inclIndicatorLinePaint.setAntiAlias(true);
	}

}
