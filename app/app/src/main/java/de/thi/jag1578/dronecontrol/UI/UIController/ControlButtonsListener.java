package de.thi.jag1578.dronecontrol.UI.UIController;

import android.graphics.Rect;
import android.os.Handler;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;


public class ControlButtonsListener implements View.OnTouchListener
{
	private ControlsViewController controlsViewController;
	private SparseArray<Handler>   handlerList;
	private SparseArray<Runnable>  actionList;
	private SparseArray<Rect>      rectList;

	public ControlButtonsListener(ControlsViewController controlsViewController)
	{
		super();

		this.controlsViewController = controlsViewController;
		this.handlerList = new SparseArray<>();
		this.actionList = new SparseArray<>();
		this.rectList = new SparseArray<>();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		Handler  actionHandler;
		Runnable action;
		Rect     buttonRect;
		Integer  tag = (Integer) v.getTag();

		switch (event.getAction())
		{
			case MotionEvent.ACTION_DOWN:

				if (this.handlerList.get(tag) != null)
				{
					return true;
				}

				// Store the rect of the button for detection of outside-drags while pressing
				buttonRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
				this.rectList.put(tag, buttonRect);

				actionHandler = new Handler();
				this.handlerList.put(tag, actionHandler);
				action = new KeepButtonPressedTask(this.controlsViewController,
				                                   actionHandler,
				                                   tag);
				this.actionList.put(tag, action);
				actionHandler.postDelayed(action, 100);

				v.getBackground().setAlpha(128);

				break;

			case MotionEvent.ACTION_MOVE:
				buttonRect = this.rectList.get(tag);
				if (buttonRect.contains(
						v.getLeft() + (int) event.getX(),
						v.getTop() + (int) event.getY()))
				{
					break;
				}
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:

				if (this.handlerList.get(tag) == null)
				{
					return true;
				}

				actionHandler = this.handlerList.get(tag);
				action = this.actionList.get(tag);
				actionHandler.removeCallbacks(action);
				this.handlerList.delete(tag);
				this.actionList.delete(tag);
				v.getBackground().setAlpha(255);
				break;
		}
		return true;
	}

}
