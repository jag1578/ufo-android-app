package de.thi.jag1578.dronecontrol.Data.DataSources;

import java.util.ArrayList;
import java.util.List;

import de.thi.jag1578.dronecontrol.Data.DroneCommunicationController;
import de.thi.jag1578.dronecontrol.UI.UIViews.DroneTile;
import de.thi.jag1578.dronecontrol.UI.UIViews.DroneTrackTile;
import de.thi.jag1578.dronecontrol.UI.UIViews.MapView;
import de.thi.jag1578.dronecontrol.UI.UIViews.ObjectiveTile;
import de.thi.jag1578.dronecontrol.UI.UIViews.ObstacleTile;
import de.thi.jag1578.dronecontrol.UI.UIViews.Tile;

import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class MapStorage extends DroneCommunicationController
{
	private ArrayList<Tile> tileArrayList;
	private DroneTile       lastDroneTile;
	private ObjectiveTile   lastObjectiveTile;


	public MapStorage()
	{
		super();

		this.tileArrayList = new ArrayList<>();

		// Make sure there is a drone-tile
		this.lastDroneTile = new DroneTile(0, 0);

		// Simulation initializes with +90°, but a north-heading is preferred
		int i = 90;
		while (i > 0)
		{
			this.dataLink.decD();
			i--;
		}

	}

	public ArrayList<Tile> getTileArrayList()
	{
		return this.tileArrayList;
	}

	public ArrayList<Tile> getTileArrayListForCenterViewPoint()
	{
		ArrayList<Tile> visibleTiles = new ArrayList<>();

		for (Tile currentTile : this.tileArrayList)
		{
			if (currentTile.getShallBeDrawn())
			{
				visibleTiles.add(currentTile);
			}

		}

		// Remove the invisible tiles from internal storage
		this.tileArrayList = visibleTiles;

		// TODO: return only tiles that are visible around the center point at current scale
		return this.tileArrayList;
	}

	public DroneTile getLastDroneTile()
	{
		return this.lastDroneTile;
	}

	public double getLastDroneX()
	{
		return this.lastDroneTile.getX();
	}

	public double getLastDroneY()
	{
		return this.lastDroneTile.getY();
	}

	public void reload()
	{
		this.updatePlayerPosition();
		this.updateObjectiveFromRadar();
		this.updateObstacleFromRadar();
	}


	private void updatePlayerPosition()
	{
		// Check whether the drone moved in X or Y direction
		if (this.dataLink.getX() != this.lastDroneTile.getX()
				|| this.dataLink.getY() != this.lastDroneTile.getY())
		{
			// Use the current drone position (before update) to create drone-track-tile
			// This is only necessary when the drone is moving, otherwise the track-tile might be
			// at the same position as the current-position-tile!
			DroneTrackTile droneTrackTile = new DroneTrackTile(
					this.dataLink.getX(),
					this.dataLink.getY());
			this.tileArrayList.add(droneTrackTile);

			// Update current drone position
			this.lastDroneTile.setX(this.dataLink.getX());
			this.lastDroneTile.setY(this.dataLink.getY());
		}

	}

	private void updateObjectiveFromRadar()
	{
		// Make sure there is an objective-tile
		if (this.lastObjectiveTile == null)
		{
			this.lastObjectiveTile = new ObjectiveTile(0, 0);
		}

		String objectiveCoordinatesString = this.dataLink.getC();

		// Check whether an objective was detected
		if (objectiveCoordinatesString.isEmpty())
		{
			// No objective detected -> remove from tile-records, if displayed in last tick!
			this.tileArrayList.remove(this.lastObjectiveTile);
		}
		else
		{
			// Objective Coordinates String is not null, so there are coordinates available
			// Parse coordinates from string and update the Objective Tile
			// In order to display the objective, add it to the tile-records!
			// Do not forget to store the index within the tile-records for eventual removal

			String coordinates[] = objectiveCoordinatesString.split("\\r\\n|\\n|\\r");
			this.lastObjectiveTile.setX(Double.parseDouble(coordinates[0]));
			this.lastObjectiveTile.setY(Double.parseDouble(coordinates[1]));

			this.tileArrayList.add(this.lastObjectiveTile);
		}

	}

	private void updateObstacleFromRadar()
	{
		double distance = this.dataLink.getR();
		int    heading  = this.dataLink.getD();

		// Check whether the radar detected an obstacle
		if (distance != -1)
		{
			// Build ObstacleTile from Distance and Heading
			ObstacleTile newObstacleTile = new ObstacleTile(
					this.calculateObstacleX(MapView.roundCoordinate(
							this.lastDroneTile.getX()),
					                        distance,
					                        heading),
					this.calculateObstacleY(MapView.roundCoordinate(
							this.lastDroneTile.getY()),
					                        distance,
					                        heading));

			// Add to the tile-records, if not detected previously
			if (!this.tileArrayList.contains(newObstacleTile))
			{
				this.tileArrayList.add(newObstacleTile);
			}

		}

	}

	private double calculateObstacleX(int droneX, double distance, int heading)
	{
		return droneX + (distance * cos(90 - heading));
	}

	private double calculateObstacleY(int droneY, double distance, int heading)
	{
		return droneY + (distance * sin(90 - heading));
	}

}
