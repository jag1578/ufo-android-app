package de.thi.jag1578.dronecontrol.Data;

public class DroneCommunicationController
{
	protected Ufosim dataLink;

	public DroneCommunicationController()
	{
		this.dataLink = Ufosim.getInstance();
	}

	public double getCurrentScale()
	{
		// TODO check this
		if(this.dataLink.getV() > 4)
		{
			// v can be 2-10
			// Scale of 1 shall equal v=1
			// Scale of 2 shall equal v=10
			return this.dataLink.getV() / 4;
		}

		return 1;
	}

	public int getDirectionForMapRotation()
	{
		// The map needs to move in the opposite direction of the actual heading
		int mapRotation = 360 - this.dataLink.getD();

		if(mapRotation == 360)
		{
			mapRotation = 0;
		}

		return mapRotation;
	}

	public int getInclinationForInclinationIndicatorRotation()
	{
		return  - this.dataLink.getI();
	}

	protected String parseDouble(double value)
	{
		return String.format("%.2f", value);
	}

	protected String parseInt(int value)
	{
		return String.format("%d", value);
	}

}
