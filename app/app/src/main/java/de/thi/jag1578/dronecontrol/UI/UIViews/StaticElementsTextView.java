package de.thi.jag1578.dronecontrol.UI.UIViews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


public class StaticElementsTextView extends FrameLayout
{
	private static int backgroundColor = Color.parseColor("#4e5965");
	private static int titleTextColor  = Color.parseColor("#c1c5c9");
	private static int valueTextColor  = Color.parseColor("#ffffff");
	private LinearLayout textLayout;
	private TextView     titleTextView;
	private TextView     unitTextView;
	private TextView     valueTextView;

	public StaticElementsTextView(Context context)
	{
		super(context);

		this.setBackgroundColor(StaticElementsTextView.backgroundColor);

		// Set up the layout used to vertically display the three text-views
		this.textLayout = new LinearLayout(this.getContext());
		this.textLayout.setOrientation(LinearLayout.VERTICAL);
		this.textLayout.setPadding(0, 0, 0, 0);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 114);
		params.gravity = Gravity.CENTER_HORIZONTAL;
		this.addView(this.textLayout, params);

		this.titleTextView = new TextView(this.getContext());
		this.titleTextView.setTextColor(StaticElementsTextView.titleTextColor);
		this.titleTextView.setGravity(Gravity.CENTER);
		this.titleTextView.setTextSize(10);
		params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 45);
		this.textLayout.addView(this.titleTextView, params);

		this.unitTextView = new TextView(this.getContext());
		this.unitTextView.setTextColor(StaticElementsTextView.valueTextColor);
		this.unitTextView.setGravity(Gravity.CENTER | Gravity.BOTTOM);
		this.unitTextView.setTextSize(5);
		params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 28);
		params.topMargin = 0;
		this.textLayout.addView(this.unitTextView, params);

		this.valueTextView = new TextView(this.getContext());
		this.valueTextView.setTextColor(StaticElementsTextView.valueTextColor);
		this.valueTextView.setGravity(Gravity.CENTER | Gravity.TOP);
		this.valueTextView.setTextSize(10);
		params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 35);
		params.topMargin = - 5;
		this.textLayout.addView(this.valueTextView, params);
	}

	public void setTitleText(final String title)
	{
		((Activity) this.getContext()).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				titleTextView.setText(title);
			}
		});
	}

	public void setUnitText(final String unit)
	{
		((Activity) this.getContext()).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				unitTextView.setText(unit);
			}
		});
	}

	public void setValueText(final String value)
	{
		((Activity) this.getContext()).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				valueTextView.setText(value);
			}
		});

	}

}
