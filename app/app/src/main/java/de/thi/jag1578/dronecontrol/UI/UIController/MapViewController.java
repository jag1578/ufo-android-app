package de.thi.jag1578.dronecontrol.UI.UIController;


import android.graphics.Canvas;

import java.util.ArrayList;

import de.thi.jag1578.dronecontrol.Data.DataSources.MapStorage;
import de.thi.jag1578.dronecontrol.Data.DroneCommunicationController;
import de.thi.jag1578.dronecontrol.UI.GameObject;
import de.thi.jag1578.dronecontrol.UI.UIViews.DroneTile;
import de.thi.jag1578.dronecontrol.UI.UIViews.MapView;
import de.thi.jag1578.dronecontrol.UI.UIViews.Tile;


public class MapViewController extends DroneCommunicationController implements GameObject
{
	private ArrayList<Tile> tileArrayList;
	private MapStorage      mapStorage;
	private MapView         mapView;
	private DroneTile       lastDroneTile;

	public MapViewController()
	{
		super();

		this.tileArrayList = new ArrayList<>();
		this.mapStorage = new MapStorage();
		this.mapView = new MapView(110);
	}

	public MapView getMapView()
	{
		return this.mapView;
	}


	public void update()
	{
		// Update the Map Storage
		this.mapStorage.reload();

		// Get all visible tiles around the current center (drone-position)
		this.tileArrayList = this.mapStorage.getTileArrayListForCenterViewPoint();
		this.lastDroneTile = this.mapStorage.getLastDroneTile();
	}

	public void draw(Canvas canvas)
	{
		// Tell the Map what the scale at which to draw
		this.mapView.setScale(this.getCurrentScale());

		// Tell the Map what Tiles to draw
		this.mapView.setTileArrayList(this.tileArrayList);
		this.mapView.setDroneTile(this.lastDroneTile);

		// Re-draw the Map
		this.mapView.drawMapInCanvas(canvas);
	}

}
