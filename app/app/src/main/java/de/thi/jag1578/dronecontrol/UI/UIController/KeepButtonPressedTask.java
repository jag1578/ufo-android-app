package de.thi.jag1578.dronecontrol.UI.UIController;


import android.os.Handler;


public class KeepButtonPressedTask implements Runnable
{
	private ControlsViewController controlsViewController;
	private Handler                handler;
	private Integer                tag;
	private Integer                currentDegrees = 1;

	KeepButtonPressedTask(ControlsViewController controlsViewController,
	                      Handler handler,
	                      Integer tag)
	{
		this.controlsViewController = controlsViewController;
		this.handler = handler;
		this.tag = tag;
	}

	public void run()
	{
		switch (tag)
		{
			case 0:
				this.controlsViewController.increaseInclination(this.currentDegrees);
				break;
			case 1:
				this.controlsViewController.increaseDirection(this.currentDegrees);
				break;
			case 2:
				this.controlsViewController.decreaseInclination(this.currentDegrees);
				break;
			case 3:
				this.controlsViewController.decreaseDirection(this.currentDegrees);
				break;
			case 4:
				this.controlsViewController.increaseSpeed();
				break;
			case 5:
				this.controlsViewController.decreaseSpeed();
				break;
		}

		switch (tag)
		{
			case 0:
			case 1:
			case 2:
			case 3:
				if(this.currentDegrees < 10)
				{
					this.currentDegrees++;
				}
				break;
		}


		this.handler.postDelayed(this, 75);
	}

}
