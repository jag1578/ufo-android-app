/* Ufo
 *
 * Simulation of Unified Flying Objects
 *
 * Copyright (C) 2013-2017 R. Gold
 *
 * This file is intended for use as an example, and
 * may be used, modified, or distributed in source or
 * object code form, without restriction, as long as
 * this copyright notice is preserved.
 *
 * The code and information is provided "as-is" without
 * warranty of any kind, either expressed or implied.
 *
 * Version: 3.0
 *
 * Do not alter this file! 
 *  
 * Use only the public interface:
 * 
 * public static Ufosim getInstance()
 * public double getX()
 * public double getY()
 * public double getZ()
 * public double getV()
 * public int getD()
 * public int getI()
 * public double getR()
 * public String getC()
 * public void incV()
 * public void decV()
 * public void incD()
 * public void decD()
 * public void incI()
 * public void decI()
 * public void reset()
 */

package de.thi.jag1578.dronecontrol.Data;

// *** class for the simulation of an ufo *************************************
public final class Ufosim implements Runnable {

  // *** constants that can be used to configure the ufo **********************
  // a)
  /*private final static double velStep = 0.05;         // step for changing the velocity
  private final static double velMax = 10;            // maximal velocity
  private final static double radarRange = 10;        // range of the radar
  private final static double detectionAngle = 0.174; // angle of the detection of the hidden object
                              // cos(80 degrees) = 0.174, i.e., 80 degrees from normal vector
  private final static double detectionRange = 5;     // range of the detection of the hidden object
                              // for correct detection, detectionRange <= radarRange
                              // optimal altitude: 5m * cos 80 = 0.87m
                              // detection radius: 5m * sin 80 = 4.92m

  // defines and detects the obstacles
  private double detectObstacleAdapter() {
    double r =  detectObstacle(  -5, -0.5,   0,  0.2,   0, 2.3);  
    r =  min(r, detectObstacle( 0.5,    5,   0,  0.2,   0, 2.3)); 
    r =  min(r, detectObstacle(-0.5,  0.5,   0,  0.2,   2, 2.3)); // front wall with door
    r =  min(r, detectObstacle(-4.8,    3,   5,  5.2,   0, 2.3));  
    r =  min(r, detectObstacle(   4,  4.8,   5,  5.2,   0, 2.3)); 
    r =  min(r, detectObstacle(   3,    4,   5,  5.2,   2, 2.3)); // middle wall with door
    r =  min(r, detectObstacle(  -5,    5,  10, 10.2,   0, 2.3)); // back wall
    r =  min(r, detectObstacle(  -5, -4.8, 0.2,   10,   0, 2.3)); // left wall
    r =  min(r, detectObstacle( 4.8,    5, 0.2,   10,   0, 2.3)); // right wall
    r =  min(r, detectObstacle(  -5,    5,   0, 10.2, 2.3, 2.5)); // roof
    
    return r;
  }

  // defines and detects the hidden object
  private String detectObjectAdapter() {
    return detectObject(4.75, 9, 0);                              // in right back corner
  }*/

  // b)
  private final static double velStep = 1;            // step for changing the velocity
  private final static double velMax = 10;            // maximal velocity
  private final static double radarRange = 110;       // range of the radar
  private final static double detectionAngle = 0.423; // angle of the detection of the hidden object
                              // cos(65 degrees) = 0.423, i.e., 65 degrees from normal vector
  private final static double detectionRange = 110;   // range of the detection of the hidden object
                              // for correct detection, detectionRange <= radarRange
                              // optimal altitude: 110m * cos 65 = 46.49m
                              // detection radius: 110m * sin 65 = 99.69m

  // defines and detects the obstacles
  private double detectObstacleAdapter() {
    double r =  detectObstacle(  50,   70,  180,  200,  0, 200);  // tv tower
    r =  min(r, detectObstacle( 200,  250,  200,  250,  0, 800)); // kalif's tower 
    r =  min(r, detectObstacle(-200, -180, -100,  -80,  0,  70)); // hell's gate left 
    r =  min(r, detectObstacle(   0,   20, -100,  -80,  0,  70)); // hell's gate right 
    r =  min(r, detectObstacle(-200,   20, -100,  -80, 40,  50)); // hell's gate horizontal 
    r =  min(r, detectObstacle(  60,  160,   40,   55,  0,  70)); // building 
    r =  min(r, detectObstacle(-250, -200,  100,  150,  0,  70)); // building 
    r =  min(r, detectObstacle(-150, -100,  100,  150,  0,  70)); // building 
    r =  min(r, detectObstacle( -50,    0,  100,  150,  0,  70)); // building 
    
    return r;
  }

  // defines and detects the hidden object
  private double diesel = -250;
  private boolean dieselNE = true;
  
  private String detectObjectAdapter() {
    if (dieselNE) { if (diesel < 250)  diesel = diesel + 0.1; else dieselNE = false; }
    else          { if (diesel > -250) diesel = diesel - 0.1; else dieselNE = true; }

    return detectObject(diesel, diesel, 0);                       // the last diesel
  }
  // **************************************************************************

  // *** do not change anything beyond this border ****************************

  private static Ufosim instance;      // singleton instance
  
  private double x;                    // x coordinate
  private double y;                    // y coordinate
  private double z;                    // z coordinate
  private int v;                       // velocity = v * velStep, 0 <= velocity <= velMax
  private int d;                       // 0 <= direction <= 359
  private int i;                       // 0 <= inclination <= 180
  private double r;                    // distance of radar detected ground or obstacle
  private String c;                    // x/y coordinate of the hidden object 
  private double xvect;                // flight vector in x direction
  private double yvect;                // flight vector in y direction
  private double zvect;                // flight vector in z direction

  // constructor
  private Ufosim() {
    x = 0;
    y = 0;
    z = 0;
    v = 0;
    d = 90;
    i = 0;
    r = -1;
    c = "";
    xvect = 0;
    yvect = 0;
    zvect = 0;

    // start the thread that calculates the coordinates
    new Thread(this).start();
  }

  // get the instance
  public static Ufosim getInstance() {
    if (instance == null)
      instance = new Ufosim();

    return instance;
  }

  // get and set
  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  public double getZ() {
    return z;
  }

  public double getV() {
    return v * velStep;
  }

  public int getD() {
    return d;
  }

  public int getI() {
    return i;
  }

  public void incV() {
    if (z >= 0 && v * velStep < velMax)
      v++;
  }

  public void decV() {
    if (z >= 0 && v > 0)
      v--;
  }

  public void incD() {
    if (z >= 0) {
      if (d == 359)
        d = 0;
      else
        d++;
    }
  }

  public void decD() {
    if (z >= 0) {
      if (d == 0)
        d = 359;
      else
        d--;
    }
  }

  public void incI() {
    if (z >= 0 && i < 180)
      i++;
  }

  public void decI() {
    if (z >= 0 && i > 0)
      i--;
  }

  public double getR() {
    return r;
  }
 
  public String getC() {
    return c;
  }

  public void reset() {
    x = 0;
    y = 0;
    z = 0;
    v = 0;
    d = 90;
    i = 0;
    r = -1;
    c = "";
  }

  // thread function
  public void run() {
    while (true) {
      // convert flight vector from polar coordinates to cartesian coordinates
      xvect = Math.sin((double)i/180 * Math.PI) * Math.cos((double)d/180 * Math.PI);
      yvect = Math.sin((double)i/180 * Math.PI) * Math.sin((double)d/180 * Math.PI);
      zvect = Math.cos((double)i/180 * Math.PI);

      // calculate new position every 100 ms with 1/10 of v
      x = x + v * velStep / 10 * xvect;
      y = y + v * velStep / 10 * yvect;
      z = z + v * velStep / 10 * zvect;

      // stop if landed or crashed
      if (z <= 0) {
        if (0 < v && v * velStep <= 1) {         // landed with slow velocity
          z = 0;
          v = 0;
          r = -1;
        }
        else if (v * velStep > 1) {              // crashed to the ground
          v = 0;
          r = -1;
        }
      }
      else {
        // detect obstacles with the radar
        r = detectObstacleAdapter();  

        if (r == 0) {                   // crashed into obstacle
          z = -0.1;
          v = 0;
          r = -1;
        }
        else {                          // detect ground with the radar
          r = min(r, detectGround(zvect));
          c = detectObjectAdapter();    // detect the hidden object
        }
      }
 
      try  {
        // Seems to be a bit too slow: sleep only 90 ms to speed up
        Thread.sleep(90);
      }
      catch (InterruptedException ex) { }
    }
  }

  // *** private detection functions ******************************************

  // detect whether the obstacle is within the radar interval s = 0, t = radarRange
  // this is a very sophisticated algorithm, do not try to understand it
  private double detectObstacle(double x1, double x2, 
                                double y1, double y2, 
                                double z1, double z2) {
    // crashed into the obstacle?
    if (x1 <= x && x <= x2 && y1 <= y && y <= y2 && z1 <= z && z <= z2) return 0;

    // calculate the x-parameter interval that intersects the obstacle
    double sx, tx, temp;

    // no x-flight? look whether x is in the x-interval or not
    if (xvect == 0) {
      if (x1 <= x && x <= x2) { sx = 0; tx = radarRange; } else return -1;
    }
    else {
      sx = (x1 - x) / xvect;
      tx = (x2 - x) / xvect;

      // borders of x-parameter interval wrongly odered?
      if (tx < sx) { temp = sx; sx = tx; tx = temp;}

      // x-parameter interval left or right of radar interval?
      if (tx < 0 || radarRange < sx) return -1;
    }

    // calculate the y-parameter interval that intersects the obstacle
    double sy, ty;

    if (yvect == 0) {
      if (y1 <= y && y <= y2) { sy = 0; ty = radarRange; } else return -1;
    }
    else {
      sy = (y1 - y) / yvect;
      ty = (y2 - y) / yvect;
      if (ty < sy) { temp = sy; sy = ty; ty = temp; }
      if (ty < 0 || radarRange < sy) return -1;
    }

    // calculate the z-parameter interval that intersects the obstacle
    double sz, tz;

    if (zvect == 0) {
      if (z1 <= z && z <= z2) { sz = 0; tz = radarRange; } else return -1;
    }
    else {
      sz = (z1 - z) / zvect;
      tz = (z2 - z) / zvect;
      if (tz < sz) { temp = sz; sz = tz; tz = temp; }
      if (tz < 0 || radarRange < sz) return -1;
    }

    // calculate intersection of parameter intervals
    double s, t;

    if (sx > sy) s = (sx > sz ? sx : sz);
    else         s = (sy > sz ? sy : sz);

    if (tx < ty) t = (tx < tz ? tx : tz);
    else         t = (ty < tz ? ty : tz);

    if (s <= t && 0 <= s && s <= radarRange) return s; else return -1;
  }

  // detect whether the ground is within the radar interval s = 0, t = radarRange
  private double detectGround(double zvect) {
    double sz = (0 - z) / zvect;

    if (0 <= sz && sz <= radarRange)  // detected
      return sz;
    else
      return -1;
  }

  // detect whether the hidden object is within the detection range 
  // this is a very sophisticated algorithm, do not try to understand it
  String detectObject(double xo, double yo, double zo) {
    // vector to the hidden object and the distance
    double objectx = xo - x;
    double objecty = yo - y;
    double objectz = zo - z;
    double distance = Math.sqrt(objectx*objectx + objecty*objecty + objectz*objectz);

    // object already reached
    if (distance <= 0.1)
      //return "found";
      return String.format("%.1f", xo) + "\n" + String.format("%.1f", yo);

    // above object
    //if (Math.sqrt(objectx*objectx + objecty*objecty) <= 0.1)
      //return "above";

    // normalize the vector
    objectx = objectx / distance;
    objecty = objecty / distance;
    objectz = objectz / distance; 

    // object above ufo or not in detection angle or not in detection range
    if (-objectz < 0 || -objectz < detectionAngle || distance > detectionRange)
      return "";

    // backup flight vector to reuse detectObstacle
    double tempx = xvect;
    double tempy = yvect;
    double tempz = zvect;

    xvect = objectx;
    yvect = objecty;
    zvect = objectz; 

    double ro = detectObstacleAdapter();

    // restore flight vector
    xvect = tempx;
    yvect = tempy;
    zvect = tempz; 

    // calculate direction to object
    //int dio = (int) (Math.acos(objectx)/Math.PI*180);
    //if (objecty < 0)
    //  dio = dio + 180;

    // no detection by radar means no obstacle between ufo and object, 
    // detection in greater distance means obstacle behind ufo
    if (ro == -1 || ro > distance) 
      return String.format("%.1f", xo) + "\n" + String.format("%.1f", yo);
    else 
      return ""; 
  }
  
  // a special minimum function
  private double min(double z1, double z2) {
    if (z1 != -1) {
      if (z2 != -1) 
        return (z1 < z2 ? z1 : z2);
      else
        return z1;
    }
    else
      return z2;
  }

}
