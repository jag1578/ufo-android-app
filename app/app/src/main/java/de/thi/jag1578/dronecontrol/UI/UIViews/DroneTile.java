package de.thi.jag1578.dronecontrol.UI.UIViews;


import android.graphics.Color;
import android.graphics.Paint;


public class DroneTile extends Tile
{
	private static int tileColor = Color.parseColor("#1a2937");

	public DroneTile(double x, double y)
	{
		super(x, y);
	}

	// Just for development reasons, in final version, a triangle shall be on top, so this
	// would not be visible at all
	public Paint setUpPaint()
	{
		Paint p = new Paint();
		p.setStyle(Paint.Style.FILL);
		p.setColor(tileColor);
		p.setAntiAlias(true);

		return p;
	}

}
