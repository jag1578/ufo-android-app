package de.thi.jag1578.dronecontrol.UI.UIController;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import de.thi.jag1578.dronecontrol.Data.DroneCommunicationController;
import de.thi.jag1578.dronecontrol.R;


public class ControlsViewController extends DroneCommunicationController
{
	private Context                context;
	private ControlButtonsListener controlButtonsListener;
	private LinearLayout           gameWidgets;
	private LinearLayout           controlsLayout;
	private RelativeLayout         controlsLeftCrossLayout;
	private LinearLayout           controlsRightCrossLayout;
	private Button                 increaseSpeedButton;
	private Button                 decreaseSpeedButton;
	private Button                 increaseDirectionButton;
	private Button                 decreaseDirectionButton;
	private Button                 increaseInclinationButton;
	private Button                 decreaseInclinationButton;

	public ControlsViewController(Context context, LinearLayout gameWidgets)
	{
		super();

		this.context = context;
		this.gameWidgets = gameWidgets;
		this.controlButtonsListener = new ControlButtonsListener(this);
		this.setUpButtons();
	}

	private void setUpButtons()
	{
		WindowManager wm      = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display       display = wm.getDefaultDisplay();
		Point         size    = new Point();
		display.getSize(size);
		int screenWidth  = size.x;
		int screenHeight = size.y;

		// Configure the controls layout for the controls
		this.controlsLayout = new LinearLayout(this.context);
		this.controlsLayout.setOrientation(LinearLayout.HORIZONTAL);
		this.controlsLayout.setPadding(49, 0, 49, 0);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 287);
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.topMargin = (screenHeight / 2) - (114 + (287 / 2));
		this.gameWidgets.addView(this.controlsLayout, params);

		// Configure the controls layout for the controls
		this.controlsLeftCrossLayout = new RelativeLayout(this.context);
		RelativeLayout.LayoutParams relParams = new RelativeLayout.LayoutParams(287, 287);
		this.controlsLayout.addView(this.controlsLeftCrossLayout, relParams);

		// Configure the controls layout for the controls
		this.controlsRightCrossLayout = new LinearLayout(this.context);
		this.controlsRightCrossLayout.setOrientation(LinearLayout.VERTICAL);
		this.controlsRightCrossLayout.setPadding((287 - 50) / 2, 0, (287 - 50) / 2, 0);
		params = new LinearLayout.LayoutParams(287, 287);
		params.leftMargin = screenWidth - ((params.width + 49) * 2);
		this.controlsLayout.addView(this.controlsRightCrossLayout, params);

		// Create and set up the button to increase direction
		this.increaseDirectionButton = new Button(this.context);
		this.increaseDirectionButton.setTag(1);
		this.increaseDirectionButton.setOnTouchListener(this.controlButtonsListener);
		this.increaseDirectionButton.setBackgroundResource(R.drawable.left_control_cross_left);
		relParams = new RelativeLayout.LayoutParams(123, 50);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relParams.topMargin = (287 - relParams.height) / 2;
		this.controlsLeftCrossLayout.addView(this.increaseDirectionButton, relParams);

		// Create and set up the button to decrease direction
		this.decreaseDirectionButton = new Button(this.context);
		this.decreaseDirectionButton.setTag(3);
		this.decreaseDirectionButton.setOnTouchListener(this.controlButtonsListener);
		this.decreaseDirectionButton.setBackgroundResource(R.drawable.left_control_cross_right);
		relParams = new RelativeLayout.LayoutParams(123, 50);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relParams.topMargin = (287 - relParams.height) / 2;
		this.controlsLeftCrossLayout.addView(this.decreaseDirectionButton, relParams);

		// Create and set up the button to increase inclination
		this.increaseInclinationButton = new Button(this.context);
		this.increaseInclinationButton.setTag(0);
		this.increaseInclinationButton.setOnTouchListener(this.controlButtonsListener);
		this.increaseInclinationButton.setBackgroundResource(R.drawable.left_control_cross_up);
		relParams = new RelativeLayout.LayoutParams(50, 123);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		relParams.leftMargin = (287 - relParams.width) / 2;
		this.controlsLeftCrossLayout.addView(this.increaseInclinationButton, relParams);

		// Create and set up the button to decrease inclination
		this.decreaseInclinationButton = new Button(this.context);
		this.decreaseInclinationButton.setTag(2);
		this.decreaseInclinationButton.setOnTouchListener(this.controlButtonsListener);
		this.decreaseInclinationButton.setBackgroundResource(R.drawable.left_control_cross_down);
		relParams = new RelativeLayout.LayoutParams(50, 123);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relParams.leftMargin = (287 - relParams.width) / 2;
		this.controlsLeftCrossLayout.addView(this.decreaseInclinationButton, relParams);

		// Create and set up the button to increase speed
		this.increaseSpeedButton = new Button(this.context);
		this.increaseSpeedButton.setTag(4);
		this.increaseSpeedButton.setOnTouchListener(this.controlButtonsListener);
		this.increaseSpeedButton.setBackgroundResource(R.drawable.speed_control_button);
		params = new LinearLayout.LayoutParams(50, 124);
		this.controlsRightCrossLayout.addView(this.increaseSpeedButton, params);

		// Create and set up the button to decrease speed
		this.decreaseSpeedButton = new Button(this.context);
		this.decreaseSpeedButton.setTag(5);
		this.decreaseSpeedButton.setOnTouchListener(this.controlButtonsListener);
		this.decreaseSpeedButton.setBackgroundResource(R.drawable.speed_control_button);
		params = new LinearLayout.LayoutParams(50, 124);
		params.topMargin = 287 - (params.height * 2);
		this.controlsRightCrossLayout.addView(this.decreaseSpeedButton, params);
	}

	public void increaseSpeed()
	{
		this.dataLink.incV();
	}

	public void decreaseSpeed()
	{
		this.dataLink.decV();
	}

	public void increaseDirection(int degrees)
	{
		int i = 0;

		while (i < degrees)
		{
			this.dataLink.decD();

			i++;
		}

	}

	public void decreaseDirection(int degrees)
	{
		int i = 0;

		while (i < degrees)
		{
			this.dataLink.incD();

			i++;
		}

	}

	public void increaseInclination(int degrees)
	{
		int i = 0;

		while (i < degrees)
		{
			this.dataLink.incI();

			i++;
		}

	}

	public void decreaseInclination(int degrees)
	{

		int i = 0;

		while (i < degrees)
		{
			this.dataLink.decI();

			i++;
		}

	}

}
