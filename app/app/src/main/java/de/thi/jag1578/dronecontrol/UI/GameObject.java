package de.thi.jag1578.dronecontrol.UI;

import android.graphics.Canvas;


/**
 * Taken from: https://github.com/romuva/game-loop/tree/master/app/src/main/java/com/ailaikvis/economywars/economywars
 * <p>
 * Created by D on 2017-11-18.
 */

public interface GameObject
{
	public void draw(Canvas canvas);

	public void update();
}