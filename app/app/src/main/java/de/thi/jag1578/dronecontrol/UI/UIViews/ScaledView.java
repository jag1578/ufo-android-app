package de.thi.jag1578.dronecontrol.UI.UIViews;


public class ScaledView
{
	private double scale = 1;

	public double getScale()
	{
		return this.scale;
	}

	public void setScale(double scale)
	{
		this.scale = 1 / scale;
	}

	protected int scaleValue(int value)
	{
		return (int) Math.round(this.getScale() * value);
	}

	protected double scaleValueAsDouble(int value)
	{
		return this.scale * value;
	}

	protected float scaleValueAsFloat(int value)
	{
		return (float) this.scale * value;
	}

}
