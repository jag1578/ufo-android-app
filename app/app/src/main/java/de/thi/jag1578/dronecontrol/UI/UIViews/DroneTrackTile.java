package de.thi.jag1578.dronecontrol.UI.UIViews;

import android.graphics.Color;
import android.graphics.Paint;


public class DroneTrackTile extends Tile
{
	private static int tileColor = Color.parseColor("#9e9b9b");
	private static int tileShadowColor = Color.parseColor("#083D78");

	public DroneTrackTile(double x, double y)
	{
		super(x, y);
	}

	public Paint setUpPaint()
	{
		Paint p = new Paint();
		p.setStyle(Paint.Style.FILL);
		p.setColor(tileColor);
		//p.setShadowLayer(8, 0, 0, DroneTrackTile.tileShadowColor);

		return p;
	}

}
